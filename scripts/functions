#!/bin/bash
#
# Useful functions for LIGO monitors

CONDA_PATH="/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda"
CONDA_ENV_NAME="ligo-summary-3.10"

activate_conda_environment() {
    local env=$1
    shift 1 || env="${CONDA_ENV_NAME}"
    . ${CONDA_PATH}/etc/profile.d/conda.sh
    conda activate ${CONDA_ENV_NAME}
}

nagios_status() {
    local exitcode=$1
    local message=$2
    local timeout=$3
    local timeoutmessage=$4
    echo -n "{\"created_gps\": $(python -m gwpy.time now), \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"$message\", \"num_status\": $exitcode}"
    if [ -n "$timeout" ]; then
        echo -n ", {\"start_sec\": $timeout, \"txt_status\": \"$timeoutmessage\", \"num_status\": 3}"
    fi
    echo -n " ]}"
}

gps_start_yesterday() {
    python -c "import datetime; from gwpy.time import tconvert; print(tconvert((datetime.datetime.utcnow() - datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)))"
}

gps_start_today() {
    python -c "import datetime; from gwpy.time import tconvert; print(tconvert(datetime.datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0)))"
}

yyyymmdd() {
    python -m gwpy.time -f %Y%m%d $1
}
